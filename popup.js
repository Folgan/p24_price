var swch1 = true;

function turnOnSwitch(on) {
	if (!on) {
		$('#swch').text('OFF');
		$('#swch').attr('class', 'switchOff');
		$('#pct').attr('src', 'icons/48x48_disabled.png');
		$('#bank_name').attr('style', 'font-weight: bold;color:grey;');
		$('#usd').attr('style', 'color:grey;');
		$('#euro').attr('style', 'color:grey;');
		saveSwitchState(false);

	} else {
		$('#swch').text('ON');
		$('#swch').attr('class', 'switchOn');
		$('#pct').attr('src', 'icons/48x48.png');
		$('#bank_name').attr('style', 'font-weight: bold;color:black;');
		$('#usd').attr('style', 'color:black;');
		$('#euro').attr('style', 'color:black;');
		saveSwitchState(true);
	}

};
function saveSwitchState(on) {
	swch1 = on;
	chrome.storage.sync.set({'switchState': on});
}
function loadSwitchState(callback) {
	chrome.storage.sync.get('switchState', function(obj) {
		if (chrome.runtime.lastError) {
			if (chrome.runtime.lastError.message) 
				console.log('error during loadSwitchState: '+ chrome.runtime.lastError.message)
			else
				console.log('error during loadSwitchState');

			return;
		}
		callback(obj);
	});
}

$('#swch').click(function() {
	if (swch1) {
		turnOnSwitch(false)
	}
	else {	
		turnOnSwitch(true)
	};
});

current_bank.rates(function(kurs) {
	if (kurs) {
		$('#bank_name').text(current_bank.name);
		$('#usd').text('1$ = '+kurs.usd+' UAH.');
		$('#euro').text('1€ = '+kurs.euro+' UAH.');
	}
	else
		$('#crs').text('N/A');
})

loadSwitchState(function(value){
	if (typeof value['switchState'] !== 'undefined')
		swch1 = value['switchState'];
	turnOnSwitch(swch1);
	console.log('readed value: ' + JSON.stringify(value));
});
console.log('popup.js');