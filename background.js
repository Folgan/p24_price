var currentTabId = 0;

function supportSite(b) {

	var params = b? {path: 'icons/48x48.png'} : {path:  'icons/48x48_disabled.png'};
	chrome.browserAction.setIcon(params);
}

function  tabUrlChanged(url) {
	var support = false;
	for(i in list) {
		support = support || ((isMatchHost(url, list[i].host)));
		
	}
	supportSite(support);
}

chrome.tabs.onActiveChanged.addListener(function (tabId, selectInfo){
	
	chrome.tabs.get(tabId, function(tab) {
		currentTabId = tabId;
		tabUrlChanged(tab.url)
	})

});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab){
	if (currentTabId == tabId && typeof changeInfo.url !== 'undefined') {
		// console.log("OnUpdated:" + changeInfo.url);
		tabUrlChanged(tab.url);
	}

})

console.log('background loaded');