function currency_converter(selectors, strips, rate, suffix, replace) {
	var selectors = selectors.join(', ');
	$(selectors).each(function(){
		var price = $(this).text();
		var org = price;

		strips.forEach(function(element){
			price = price.replace(element, '');
		})

		price = price.trim();

		if (typeof replace !== 'undefined') {
			replace.forEach(function(r){
				for(var from in r) {	
					price = price.replace(from, r[from]);

				}	
			})
		}

		match = price.match(/\d*\.\d*/i);
		if (match)
			price = match[0];


		if (price == '')
			$(this).text('')
		else {
			$(this).attr('title', org);
			$(this).text((parseFloat(price) * rate).toFixed(2) + suffix);
		}
	})
}

function replace(rate, filter) {
	if (typeof filter === 'undefined')
		return;
	currency_converter(filter.selectors, filter.strips, rate, ' UAH', filter.replace);
}
